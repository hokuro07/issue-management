package lib

import (
	"log"
	"net/http"
	"os"

	slackGo "github.com/slack-go/slack"
	mongodb "gitlab.com/hokuro07/issue-management/lib/mongodb"
	slack "gitlab.com/hokuro07/issue-management/lib/slack"
)

// HandleNotice : 期限切れの課題を各チャンネルに通知する
func HandleNotice(w http.ResponseWriter, r *http.Request) {

	api := slackGo.New(os.Getenv("SLACK_BOT_TOKEN"))

	client, err := mongodb.ConnectMongoDB()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// 通知対象の課題を取得
	issues, err := mongodb.FetchIssueToNotice(client)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for _, issue := range issues {
		messageOption := slack.GenerateShareBlock(issue)

		_, _, err := api.PostMessage(
			issue.ChannelID,
			messageOption,
		)
		if err != nil {
			log.Println(err)
			return
		}
	}
}
