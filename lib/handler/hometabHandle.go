package lib

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	mongodb "gitlab.com/hokuro07/issue-management/lib/mongodb"
	slack "gitlab.com/hokuro07/issue-management/lib/slack"
	slackGo "github.com/slack-go/slack"
	slackevents "github.com/slack-go/slack/slackevents"
)

// HandleHome : Hometab要求時の処理
func HandleHome(w http.ResponseWriter, r *http.Request) {
	botAPI := slackGo.New(os.Getenv("SLACK_BOT_TOKEN"))

	// signin secretを検証
	verifier, err := slackGo.NewSecretsVerifier(r.Header, os.Getenv("SLACK_SIGNING_SECRET"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	bodyReader := io.TeeReader(r.Body, &verifier)
	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := verifier.Ensure(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	eventsAPIEvent, err := slackevents.ParseEvent(json.RawMessage(body), slackevents.OptionNoVerifyToken())
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// イベント種別で処理を分ける
	switch eventsAPIEvent.Type {
	// URL検証時の処理
	case slackevents.URLVerification:
		var res *slackevents.ChallengeResponse
		if err := json.Unmarshal(body, &res); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		if _, err := w.Write([]byte(res.Challenge)); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		break

	case slackevents.CallbackEvent:

		if appHomeOpenedEvent, ok := eventsAPIEvent.InnerEvent.Data.(*slackevents.AppHomeOpenedEvent); ok {
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// ユーザIDで検索
			issues, err := mongodb.FetchIssueByUserID(client, appHomeOpenedEvent.User)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// BOTが所属するpublic,privateチャンネルを取得
			channels, err := slack.GetChannels(os.Getenv("SLACK_BOT_USER_ID"))
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// チャンネルID群のMAPを生成
			channelIDs := slack.GenerateChannelIDsMap(channels)

			// HomeTabのビューを生成
			hometabRequest := slack.GenerateHomeTab(issues, channelIDs, nil)

			// SlackにViewを公開
			_, err = botAPI.PublishView(appHomeOpenedEvent.User, hometabRequest, "")

			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		} else {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		break

	}
}
