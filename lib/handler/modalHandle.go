package lib

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	slackGo "github.com/slack-go/slack"
	mongodb "gitlab.com/hokuro07/issue-management/lib/mongodb"
	slack "gitlab.com/hokuro07/issue-management/lib/slack"
	mongo "go.mongodb.org/mongo-driver/mongo"
)

// サインイン用の秘密コードを検証
func verifySigningSecret(r *http.Request) ([]byte, error) {
	verifier, err := slackGo.NewSecretsVerifier(r.Header, os.Getenv("SLACK_SIGNING_SECRET"))
	if err != nil {
		log.Println(err)
		return nil, err
	}

	bodyReader := io.TeeReader(r.Body, &verifier)

	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(body))

	if err := verifier.Ensure(); err != nil {
		log.Println(err)
		return nil, err
	}

	return body, nil
}

// HandleModal : モーダル要求時の処理
func HandleModal(w http.ResponseWriter, r *http.Request) {
	botAPI := slackGo.New(os.Getenv("SLACK_BOT_TOKEN"))

	body, err := verifySigningSecret(r)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// payload=の文字列を除外し、unescapeする
	payload, err := url.QueryUnescape(string(body)[8:])

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var ic slackGo.InteractionCallback
	if err = json.Unmarshal([]byte(payload), &ic); err != nil {
		log.Printf(err.Error())
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	switch ic.Type {
	// メッセージからのアクション処理
	case "message_action":
		modalRequest := slackGo.ModalViewRequest{}

		switch ic.CallbackID {
		// メッセージから課題登録モーダルを表示する処理
		case "channel_shortcut_create_issue":
			// 自分（操作者）のUserIDをキーに所属するチャンネル情報を取得
			channels, err := slack.GetChannels(ic.User.ID)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 課題登録モーダルを生成
			modalRequest = slack.GenerateStoreModal(ic.Message, ic.Channel.ID, channels)
			break
		}

		// Slackへモーダルオープンを要求
		_, err = botAPI.OpenView(ic.TriggerID, modalRequest)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		break

	// モーダル内容
	case "view_submission":
		client, err := mongodb.ConnectMongoDB()
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		switch ic.View.CallbackID {
		// 保存処理
		case "storeIssue":
			var issues mongodb.Issues

			err = client.UseSession(
				context.Background(),
				func(sc mongo.SessionContext) error {
					// トランザクション開始
					err = sc.StartTransaction()
					if err != nil {
						return err
					}

					// slackからpostされた課題を保存
					if err = mongodb.StoreIssue(client, ic); err != nil {
						return err
					}

					// ユーザIDをキーに課題を取得する
					issues, err = mongodb.FetchIssueByUserID(client, ic.User.ID)
					if err != nil {
						return err
					}

					// トランザクション終了
					return sc.CommitTransaction(sc)
				},
			)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// BOTが所属するpublic,privateチャンネルを取得
			channels, err := slack.GetChannels(os.Getenv("SLACK_BOT_USER_ID"))
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// チャンネルID群のMAPを生成
			channelIDs := slack.GenerateChannelIDsMap(channels)

			// HomeTabのビューを生成
			hometabRequest := slack.GenerateHomeTab(issues, channelIDs, nil)

			// SlackにViewを公開
			_, err = botAPI.PublishView(ic.User.ID, hometabRequest, "")
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			break
		// 検索処理
		case "searchIssue":
			var issues mongodb.Issues

			values := ic.View.State.Values
			err = client.UseSession(
				context.Background(),
				func(sc mongo.SessionContext) error {
					// トランザクション開始
					err = sc.StartTransaction()
					if err != nil {
						return err
					}

					// キーワードで検索
					issues, err = mongodb.FetchIssueByModalParams(client, values)
					if err != nil {
						return err
					}

					// トランザクション終了
					return sc.CommitTransaction(sc)
				},
			)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// BOTが所属するpublic,privateチャンネルを取得
			channels, err := slack.GetChannels(os.Getenv("SLACK_BOT_USER_ID"))
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// チャンネルID群のMAPを生成
			channelIDs := slack.GenerateChannelIDsMap(channels)

			// SlackにViewを公開
			modalRequest := slack.GenerateHomeTab(issues, channelIDs, values)

			// SlackにViewを公開
			_, err = botAPI.PublishView(ic.User.ID, modalRequest, "")
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break
		default:
			// 課題 更新処理
			if strings.Contains(ic.View.CallbackID, "updateIssue") {
				var issues mongodb.Issues

				err = client.UseSession(
					context.Background(),
					func(sc mongo.SessionContext) error {
						// トランザクション開始
						err = sc.StartTransaction()
						if err != nil {
							return err
						}

						docID := string(ic.View.CallbackID)[12:]

						// slackからpostされた課題を更新
						if err = mongodb.UpdateIssue(client, ic, docID); err != nil {
							return err
						}

						// キーワードで検索
						issues, err = mongodb.FetchIssueByUserID(client, ic.User.ID)
						if err != nil {
							return err
						}

						// トランザクション終了
						return sc.CommitTransaction(sc)
					},
				)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())

				// BOTが所属するpublic,privateチャンネルを取得
				channels, err := slack.GetChannels(os.Getenv("SLACK_BOT_USER_ID"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// チャンネルID群のMAPを生成
				channelIDs := slack.GenerateChannelIDsMap(channels)

				// HomeTabのビューを生成
				hometabRequest := slack.GenerateHomeTab(issues, channelIDs, nil)

				// SlackにViewを公開
				_, err = botAPI.PublishView(ic.User.ID, hometabRequest, "")
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				// 課題とコメントを削除
			} else if strings.Contains(ic.View.CallbackID, "deleteIssue") {
				var issues mongodb.Issues

				err = client.UseSession(
					context.Background(),
					func(sc mongo.SessionContext) error {
						// トランザクション開始
						err = sc.StartTransaction()
						if err != nil {
							return err
						}

						// SlackのcallbackIDからDocIDのみを抽出
						docID := string(ic.View.CallbackID)[12:]

						// 課題を削除
						err = mongodb.DeleteIssue(client, docID)
						if err != nil {
							return err
						}

						// 課題に紐づくコメントを削除
						err = mongodb.DeleteCommentsByIssueDocID(client, docID)
						if err != nil {
							return err
						}

						// ユーザIDをキーに課題を取得する
						issues, err = mongodb.FetchIssueByUserID(client, ic.User.ID)
						if err != nil {
							return err
						}

						// トランザクション終了
						return sc.CommitTransaction(sc)
					},
				)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())

				// BOTが所属するpublic,privateチャンネルを取得
				channels, err := slack.GetChannels(os.Getenv("SLACK_BOT_USER_ID"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// チャンネルID群のMAPを生成
				channelIDs := slack.GenerateChannelIDsMap(channels)

				// HomeTabのビューを生成
				hometabRequest := slack.GenerateHomeTab(issues, channelIDs, nil)

				// SlackにViewを公開
				_, err = botAPI.PublishView(ic.User.ID, hometabRequest, "")
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// コメント 登録処理
			} else if strings.Contains(ic.View.CallbackID, "storeComment") {
				err = client.UseSession(
					context.Background(),
					func(sc mongo.SessionContext) error {
						// トランザクション開始
						err = sc.StartTransaction()
						if err != nil {
							return err
						}

						// SlackのcallbackIDからIssueDocIDのみを抽出
						issueDocID := string(ic.View.CallbackID)[13:]

						// slackからpostされたコメントを保存
						if err = mongodb.StoreComment(client, ic, issueDocID); err != nil {
							return err
						}

						// トランザクション終了
						return sc.CommitTransaction(sc)
					},
				)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())
				// コメント 更新処理
			} else if strings.Contains(ic.View.CallbackID, "updateComment") {
				var comments mongodb.Comments
				var issueDocID string

				err = client.UseSession(
					context.Background(),
					func(sc mongo.SessionContext) error {
						// トランザクション開始
						err = sc.StartTransaction()
						if err != nil {
							return err
						}

						// SlackのcallbackIDからDocIDのみを抽出
						docID := string(ic.View.CallbackID)[14:]

						// slackからpostされたコメントを更新する
						issueDocID, err = mongodb.UpdateComment(client, ic, docID)
						if err != nil {
							return err
						}

						// 課題のDocIDをキーにコメントを取得する
						comments, err = mongodb.FetchCommentByIssueDocID(client, issueDocID)
						if err != nil {
							return err
						}

						// トランザクション終了
						return sc.CommitTransaction(sc)
					},
				)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())

				// コメントモーダルを生成
				modalRequest := slack.GenerateCommentModal(issueDocID, comments, ic.User.ID)

				// Slackへモーダルオープンを要求
				_, err = botAPI.UpdateView(modalRequest, "", "", ic.View.RootViewID)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())
			} else if strings.Contains(ic.View.CallbackID, "deleteComment") {
				var comments mongodb.Comments
				var issueDocID string

				err = client.UseSession(
					context.Background(),
					func(sc mongo.SessionContext) error {
						// トランザクション開始
						err = sc.StartTransaction()
						if err != nil {
							return err
						}

						// SlackのcallbackIDからDocIDのみを抽出
						docID := string(ic.View.CallbackID)[14:]

						// slackからpostされたコメントを保存
						issueDocID, err = mongodb.DeleteComment(client, docID)
						if err != nil {
							return err
						}

						// 課題のDocIDをキーにコメントを取得する
						comments, err = mongodb.FetchCommentByIssueDocID(client, issueDocID)
						if err != nil {
							return err
						}
						// トランザクション終了
						return sc.CommitTransaction(sc)
					},
				)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())

				// コメントモーダルを生成
				modalRequest := slack.GenerateCommentModal(issueDocID, comments, ic.User.ID)

				// Slackへモーダルオープン
				_, err = botAPI.UpdateView(modalRequest, "", "", ic.View.RootViewID)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 接続を閉じる
				client.Disconnect(context.Background())
			}
			break
		}
		break

	// ブロックアクションの処理
	case "block_actions":

		switch ic.ActionCallback.BlockActions[0].ActionID {
		// ホームタブの3点メニュー処理
		case "menu":
			switch ic.ActionCallback.BlockActions[0].SelectedOption.Value {
			// 検索押下時 (検索モーダル起動)
			case "searchIssue":
				// 課題検索モーダルを作成
				modalRequest := slack.GenerateSearchModal()

				// Slackへモーダルオープンを要求
				_, err = botAPI.OpenView(ic.TriggerID, modalRequest)

				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				break
			// 作成押下時（登録モーダル起動）
			case "createIssue":
				// 自分（操作者）のUserIDをキーに所属するチャンネル情報を取得
				channels, err := slack.GetChannels(ic.User.ID)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				// 課題登録モーダルを生成
				modalRequest := slack.GenerateStoreModal(ic.Message, "", channels)

				// Slackへモーダルオープンを要求
				_, err = botAPI.OpenView(ic.TriggerID, modalRequest)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				break
			}
			break

		// 編集ボタン処理
		case "editIssue":
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// DocIDをキーに課題を取得する
			issue, err := mongodb.FetchIssueByDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// 自分（操作者）のUserIDをキーに所属するチャンネル情報を取得
			channels, err := slack.GetChannels(ic.User.ID)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 課題更新モーダルを作成
			modalRequest := slack.GenerateUpdateModal(issue, channels)

			// Slackへモーダルオープンを要求
			_, err = botAPI.OpenView(ic.TriggerID, modalRequest)

			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break
		// 削除ボタン処理
		case "deleteIssue":
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// DocIDをキーに課題を取得する
			issue, err := mongodb.FetchIssueByDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// 課題削除確認モーダルを生成
			modalRequest := slack.GenerateDeleteIssueConfirmModal(ic.ActionCallback.BlockActions[0].Value, issue.Content)

			// Slackへモーダルプッシュ
			_, err = botAPI.OpenView(ic.TriggerID, modalRequest)

			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break
		// コメントボタン処理
		case "comment":
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// DocIDをキーに課題を取得する (そもそも課題が存在するか確認)
			_, err = mongodb.FetchIssueByDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 課題のDocIDをキーにコメントを取得する
			comments, err := mongodb.FetchCommentByIssueDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// コメントモーダルを生成
			modalRequest := slack.GenerateCommentModal(ic.ActionCallback.BlockActions[0].Value, comments, ic.User.ID)

			// Slackへモーダルオープン
			_, err = botAPI.OpenView(ic.TriggerID, modalRequest)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break
		// 共有ボタン処理
		case "share":
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// ドキュメントIDで検索
			issue, err := mongodb.FetchIssueByDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// 共有ブロックを生成
			messageOption := slack.GenerateShareBlock(issue)

			_, _, err = botAPI.PostMessage(issue.ChannelID, messageOption)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break
		// コメント編集ボタン処理
		case "editComment":
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// ドキュメントIDをキーにコメントを取得する
			comment, err := mongodb.FetchCommentByDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// コメント編集モーダルを生成
			modalRequest := slack.GenerateEditCommentModal(comment)

			// Slackへモーダルプッシュ
			_, err = botAPI.PushView(ic.TriggerID, modalRequest)

			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break

		// 削除確認モーダルを表示する
		case "deleteComment":
			client, err := mongodb.ConnectMongoDB()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// コメントを取得する
			comment, err := mongodb.FetchCommentByDocID(client, ic.ActionCallback.BlockActions[0].Value)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			// 接続を閉じる
			client.Disconnect(context.Background())

			// コメント削除確認モーダルを生成
			modalRequest := slack.GenerateDeleteCommentConfirmModal(ic.ActionCallback.BlockActions[0].Value, comment.Content)

			// Slackへモーダルプッシュ
			_, err = botAPI.PushView(ic.TriggerID, modalRequest)

			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			break
		}
	}
}
