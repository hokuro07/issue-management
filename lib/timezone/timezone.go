package lib

import (
	"time"
)

// DateLayout : YYYY-MM-DD レイアウト
const DateLayout = "2006-01-02"

// DatetimeLayout : YYYY-MM-DD hh:mm:ss レイアウト
const DatetimeLayout = "2006-01-02 15:04:05"

// SetJpTimeZone : Asia/Tokyoをタイムゾーンを指定
func SetJpTimeZone() {
	time.Local = time.FixedZone("Asia/Tokyo", 9*60*60)
	time.LoadLocation("Asia/Tokyo")
}

// GetCurrentDate : 現在年月日を取得
func GetCurrentDate() string {
	cTime := time.Now()
	return cTime.Format(DateLayout)
}
