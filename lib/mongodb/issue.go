package lib

import (
	"context"
	"os"
	"time"

	slackGo "github.com/slack-go/slack"
	timezone "gitlab.com/hokuro07/issue-management/lib/timezone"
	bson "go.mongodb.org/mongo-driver/bson"
	primitive "go.mongodb.org/mongo-driver/bson/primitive"
	mongo "go.mongodb.org/mongo-driver/mongo"
	options "go.mongodb.org/mongo-driver/mongo/options"
)

// Issue : 課題
type Issue struct {
	DocID     primitive.ObjectID `json:"id" bson:"_id"`
	TeamID    string             `json:"team_id" bson:"team_id"`
	ChannelID string             `json:"channel_id" bson:"channel_id"`
	Title     string             `json:"title" bson:"title"`
	Content   string             `json:"content" bson:"content"`
	Deadline  time.Time          `json:"deadline" bson:"deadline"`
	UserID    string             `json:"user_id" bson:"user_id"`
	Status    string             `json:"status" bson:"status"`
	Creater   string             `json:"creater" bson:"creater"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	Updater   string             `json:"updater" bson:"updater"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
}

// Issues : 課題群
type Issues []Issue

// FetchIssueByModalParams : 検索ワードから課題を取得する
func FetchIssueByModalParams(client *mongo.Client, params map[string]map[string]slackGo.BlockAction) (Issues, error) {
	var issues Issues

	keyword := params["keyword"]["keyword"].Value
	userID := params["user"]["user"].SelectedUser
	status := params["status"]["status"].SelectedOption.Value

	var searchConditions bson.D
	// キーワードが指定されている場合
	if len(keyword) > 0 {
		keywordCondition := bson.E{
			"$or",
			bson.A{
				bson.D{{"title", primitive.Regex{Pattern: keyword, Options: "i"}}},
				bson.D{{"content", primitive.Regex{Pattern: keyword, Options: "i"}}},
			},
		}

		searchConditions = append(searchConditions, keywordCondition)
	}

	// 担当者が指定されている場合
	if len(userID) > 0 {
		userCondition := bson.E{
			"user_id",
			userID,
		}

		searchConditions = append(searchConditions, userCondition)
	}

	// ステータスが指定されている場合
	if len(status) > 0 {
		statusCondition := bson.E{
			"status",
			status,
		}

		searchConditions = append(searchConditions, statusCondition)
	}

	// 検索条件が指定されていない場合
	if len(keyword) == 0 && len(userID) == 0 && len(status) == 0 {
		searchConditions = bson.D{}
	}

	// issuesコレクションからドキュメントを取得
	iter, err := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").Find(
		context.Background(),
		searchConditions,
		// 作成日時 - desc
		options.Find().SetSort(
			bson.D{
				{
					"created_at",
					-1,
				},
			},
		),
	)
	if err != nil {
		return Issues{}, err
	}

	// ドキュメント分処理を繰り返す
	for iter.Next(context.Background()) {
		var issue Issue
		iter.Decode(&issue)
		issues = append(issues, issue)
	}

	return issues, nil
}

// FetchIssueByDocID : ドキュメントIDから課題を取得する
func FetchIssueByDocID(client *mongo.Client, hex string) (Issue, error) {
	var issue Issue

	objectID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return Issue{}, err
	}

	// issuesコレクションからドキュメントを取得
	err = client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").FindOne(
		context.Background(),
		bson.M{
			"_id": objectID,
		},
	).Decode(&issue)
	if err != nil {
		return Issue{}, err
	}

	return issue, nil
}

// FetchIssueByUserID : 検索ワードから課題を取得する
func FetchIssueByUserID(client *mongo.Client, userID string) (Issues, error) {
	var issues Issues

	// issuesコレクションからドキュメントを取得
	iter, err := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").Find(
		context.Background(),
		bson.M{
			"user_id": userID,
		},
		// 作成日時 - desc
		options.Find().SetSort(
			bson.D{
				{
					"created_at",
					-1,
				},
			},
		),
	)
	if err != nil {
		return Issues{}, err
	}

	// ドキュメント分処理を繰り返す
	for iter.Next(context.Background()) {
		var issue Issue
		iter.Decode(&issue)
		issues = append(issues, issue)
	}

	return issues, nil
}

// FetchIssueToNotice : 通知すべき課題を取得
func FetchIssueToNotice(client *mongo.Client) (Issues, error) {
	var issues Issues

	// issuesコレクションからドキュメントを取得
	iter, err := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").Find(
		context.Background(),
		bson.D{
			{
				"status",
				bson.D{
					{
						"$in",
						bson.A{
							"未着手",
							"着手中",
						},
					},
				},
			},
			{
				"deadline",
				bson.D{
					{
						"$lt",
						time.Now().Add(9 * time.Hour), // mongoDBのタイムゾーンの関係で+9時間して検索する
					},
				},
			},
		},
		// 作成日時 - desc
		options.Find().SetSort(
			bson.D{
				{
					"created_at",
					-1,
				},
			},
		),
	)
	if err != nil {
		return Issues{}, err
	}

	// ドキュメント分処理を繰り返す
	for iter.Next(context.Background()) {
		var issue Issue
		iter.Decode(&issue)
		issues = append(issues, issue)
	}

	return issues, nil
}

// StoreIssue : 課題を保存する
func StoreIssue(client *mongo.Client, ic slackGo.InteractionCallback) error {
	values := ic.View.State.Values

	parsedDeadline, err := time.Parse(timezone.DatetimeLayout, values["deadline"]["deadline"].SelectedDate+" 23:59:59")
	if err != nil {
		return err
	}

	_, err = client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").InsertOne(
		context.Background(),
		bson.M{
			"team_id":    ic.Team.ID,
			"channel_id": removeChannelSymbol(values["channel"]["channel"].SelectedOption.Text.Text),
			"title":      values["issueName"]["issueName"].Value,
			"content":    values["issueContent"]["issueContent"].Value,
			"deadline":   parsedDeadline,
			"user_id":    values["user"]["user"].SelectedUser,
			"status":     "未着手",
			"creater":    ic.User.ID,
			"created_at": time.Now(),
			"updater":    ic.User.ID,
			"updated_at": time.Now(),
		},
	)

	if err != nil {
		return err
	}

	return nil
}

// UpdateIssue : 課題を更新する
func UpdateIssue(client *mongo.Client, ic slackGo.InteractionCallback, hex string) error {
	values := ic.View.State.Values

	objectID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return err
	}

	parsedDeadline, err := time.Parse(timezone.DatetimeLayout, values["deadline"]["deadline"].SelectedDate+" 23:59:59")
	if err != nil {
		return err
	}

	_, err = client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").UpdateOne(
		context.Background(),
		bson.M{
			"_id": objectID,
		},
		bson.M{
			"$set": bson.M{
				"channel_id": removeChannelSymbol(values["channel"]["channel"].SelectedOption.Text.Text),
				"title":      values["issueName"]["issueName"].Value,
				"content":    values["issueContent"]["issueContent"].Value,
				"deadline":   parsedDeadline,
				"user_id":    values["user"]["user"].SelectedUser,
				"status":     values["status"]["status"].SelectedOption.Text.Text,
				"updater":    ic.User.ID,
				"updated_at": time.Now(),
			},
		},
	)

	if err != nil {
		return err
	}

	return nil
}

// DeleteIssue : 課題を削除する
func DeleteIssue(client *mongo.Client, hex string) error {
	objectID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return err
	}

	singleResult := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("issues").FindOneAndDelete(
		context.Background(),
		bson.M{
			"_id": objectID,
		},
	)

	if singleResult.Err() != nil {
		return singleResult.Err()
	}

	var issue Issue
	singleResult.Decode(&issue)

	return nil
}
