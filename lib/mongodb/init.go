package lib

import (
	"context"
	"log"
	"os"
	"strings"
	"time"

	mongo "go.mongodb.org/mongo-driver/mongo"
	options "go.mongodb.org/mongo-driver/mongo/options"
	readpref "go.mongodb.org/mongo-driver/mongo/readpref"
)

// ConnectMongoDB : MongoDBへの接続を確立
func ConnectMongoDB() (*mongo.Client, error) {
	// コンテキストの作成
	//   - バックグラウンドで接続する。タイムアウトは10秒
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// 指定したURIに接続する
	client, err := mongo.Connect(
		ctx,
		options.Client().ApplyURI(
			"mongodb+srv://"+os.Getenv("MONGODB_ID")+":"+os.Getenv("MONGODB_PW")+"@"+os.Getenv("MONGODB_HOSTNAME")+"/"+os.Getenv("MONGODB_DBNAME")+"?retryWrites=true&w=majority",
		),
	)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	// 接続を試す
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Println(":::connection error:::")
		return nil, err
	}
	return client, err
}

// チャンネルの<# >部を削除
func removeChannelSymbol(channel string) string {
	return strings.Replace(strings.Replace(channel, "<#", "", 1), ">", "", 1)
}
