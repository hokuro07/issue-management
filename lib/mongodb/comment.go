package lib

import (
	"context"
	"os"
	"time"

	slackGo "github.com/slack-go/slack"
	bson "go.mongodb.org/mongo-driver/bson"
	primitive "go.mongodb.org/mongo-driver/bson/primitive"
	mongo "go.mongodb.org/mongo-driver/mongo"
	options "go.mongodb.org/mongo-driver/mongo/options"
)

// Comment : コメント
type Comment struct {
	DocID      primitive.ObjectID `json:"id" bson:"_id"`
	IssueDocID string             `json:"issue_doc_id" bson:"issue_doc_id"`
	Content    string             `json:"content" bson:"content"`
	Creater    string             `json:"creater" bson:"creater"`
	CreatedAt  time.Time          `json:"created_at" bson:"created_at"`
	Updater    string             `json:"updater" bson:"updater"`
	UpdatedAt  time.Time          `json:"updated_at" bson:"updated_at"`
}

// Comments : コメント群
type Comments []Comment

// FetchCommentByIssueDocID : IssueのDocIDをキーにコメントを取得する
func FetchCommentByIssueDocID(client *mongo.Client, issueDocID string) (Comments, error) {
	var comments Comments

	// commentsコレクションからドキュメントを取得してくる
	iter, err := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("comments").Find(
		context.Background(),
		bson.M{
			"issue_doc_id": issueDocID,
		},
		// 作成日時 - desc
		options.Find().SetSort(
			bson.D{
				{
					"created_at",
					1,
				},
			},
		),
	)
	if err != nil {
		return Comments{}, err
	}

	// ドキュメント分処理を繰り返す
	for iter.Next(context.Background()) {
		var comment Comment
		iter.Decode(&comment)
		comments = append(comments, comment)
	}

	return comments, nil
}

// FetchCommentByDocID : ドキュメントIDからコメントを取得する
func FetchCommentByDocID(client *mongo.Client, hex string) (Comment, error) {
	var comment Comment

	objectID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return Comment{}, err
	}

	// issuesコレクションからドキュメントを取得
	err = client.Database(os.Getenv("MONGODB_DBNAME")).Collection("comments").FindOne(
		context.Background(),
		bson.M{
			"_id": objectID,
		},
	).Decode(&comment)
	if err != nil {
		return Comment{}, err
	}

	return comment, nil
}

// StoreComment : コメントを保存する
func StoreComment(client *mongo.Client, ic slackGo.InteractionCallback, issueDocID string) error {
	values := ic.View.State.Values

	_, err := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("comments").InsertOne(
		context.Background(),
		bson.M{
			"issue_doc_id": issueDocID,
			"content":      values["newComment"]["newComment"].Value,
			"creater":      ic.User.ID,
			"created_at":   time.Now(),
			"updater":      ic.User.ID,
			"updated_at":   time.Now(),
		},
	)

	if err != nil {
		return err
	}

	return nil
}

// UpdateComment : コメントを更新する
func UpdateComment(client *mongo.Client, ic slackGo.InteractionCallback, hex string) (string, error) {
	values := ic.View.State.Values

	objectID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return "", err
	}

	singleResult := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("comments").FindOneAndUpdate(
		context.Background(),
		bson.M{
			"_id": objectID,
		},
		bson.M{
			"$set": bson.M{
				"content":    values["editComment"]["editComment"].Value,
				"updater":    ic.User.ID,
				"updated_at": time.Now(),
			},
		},
	)

	if singleResult.Err() != nil {
		return "", singleResult.Err()
	}

	var comment Comment
	singleResult.Decode(&comment)

	return comment.IssueDocID, nil
}

// DeleteComment : コメントを削除する
func DeleteComment(client *mongo.Client, hex string) (string, error) {
	objectID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return "", err
	}

	singleResult := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("comments").FindOneAndDelete(
		context.Background(),
		bson.M{
			"_id": objectID,
		},
	)

	if singleResult.Err() != nil {
		return "", singleResult.Err()
	}

	var comment Comment
	singleResult.Decode(&comment)

	return comment.IssueDocID, nil
}

// DeleteCommentsByIssueDocID : 課題に紐づくコメントを削除する
func DeleteCommentsByIssueDocID(client *mongo.Client, issueDocID string) error {
	_, err := client.Database(os.Getenv("MONGODB_DBNAME")).Collection("comments").DeleteMany(
		context.Background(),
		bson.M{
			"issue_doc_id": issueDocID,
		},
	)

	if err != nil {
		return err
	}

	return nil
}
