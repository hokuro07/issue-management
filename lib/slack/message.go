package lib

import (
	"time"

	slackGo "github.com/slack-go/slack"
	mongodb "gitlab.com/hokuro07/issue-management/lib/mongodb"
	timezone "gitlab.com/hokuro07/issue-management/lib/timezone"
)

// GenerateShareBlock : 共有の投稿
func GenerateShareBlock(issue mongodb.Issue) slackGo.MsgOption {
	// blankText
	blankText := slackGo.NewTextBlockObject(slackGo.PlainTextType, " ", false, false)

	// タイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, issue.Title, false, false)
	headerText := slackGo.NewHeaderBlock(titleText)

	fieldSlice := make([]*slackGo.TextBlockObject, 0)

	// 内容
	contentText := "Created by <@" + issue.Creater + "> at " + issue.CreatedAt.In(time.Local).Format(timezone.DatetimeLayout)
	contentText += "　　Last updated by <@" + issue.Updater + "> at " + issue.UpdatedAt.In(time.Local).Format(timezone.DatetimeLayout)
	contentText += "\n\n" + issue.Content + "\n"
	contentTextObject := slackGo.NewTextBlockObject(slackGo.MarkdownType, contentText, false, false)
	contentContextBlock := slackGo.NewContextBlock("", []slackGo.MixedElement{contentTextObject}...)

	// チャンネル
	channelField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• チャンネル : `<#"+issue.ChannelID+">`", false, false)
	fieldSlice = append(fieldSlice, channelField)
	// ステータス
	statusField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• ステータス : `"+issue.Status+"`", false, false)
	fieldSlice = append(fieldSlice, statusField)
	// 期限
	deadlineField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• 期限　　　 : `"+issue.Deadline.Format(timezone.DateLayout)+"`", false, false)
	fieldSlice = append(fieldSlice, deadlineField)
	// 担当者
	userField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• 担当者　　 : `<@"+issue.UserID+">`", false, false)
	fieldSlice = append(fieldSlice, userField)

	fieldsSection := slackGo.NewSectionBlock(blankText, fieldSlice, nil)

	// 編集ボタン
	editBtnTxt := slackGo.NewTextBlockObject(slackGo.PlainTextType, "編集", false, false)
	editBtn := slackGo.NewButtonBlockElement("editIssue", issue.DocID.Hex(), editBtnTxt)

	// コメントボタン
	commentBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメント", false, false)
	commentBtnElement := slackGo.NewButtonBlockElement("comment", issue.DocID.Hex(), commentBtnText)

	actionBlock := slackGo.NewActionBlock("", editBtn, commentBtnElement)

	message := slackGo.MsgOptionBlocks(
		headerText,
		contentContextBlock,
		fieldsSection,
		actionBlock,
	)

	return message
}
