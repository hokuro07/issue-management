package lib

import (
	"fmt"
	"time"

	slackGo "github.com/slack-go/slack"
	mongodb "gitlab.com/hokuro07/issue-management/lib/mongodb"
	timezone "gitlab.com/hokuro07/issue-management/lib/timezone"
)

// チャンネルのセレクトボックスオブジェクトを生成
func generateChannelsOptionBlockObjects(channels []slackGo.Channel) []*slackGo.OptionBlockObject {
	optionBlockObjects := make([]*slackGo.OptionBlockObject, 0, len(channels))
	var text string
	for _, channel := range channels {
		text = fmt.Sprintf("<#%s>", channel.ID)

		optionText := slackGo.NewTextBlockObject(slackGo.PlainTextType, text, false, false)
		optionBlockObjects = append(optionBlockObjects, slackGo.NewOptionBlockObject(channel.ID, optionText))
	}
	return optionBlockObjects
}

// GenerateStoreModal : 課題登録モーダルを生成
func GenerateStoreModal(message slackGo.Message, currentChannel string, channels []slackGo.Channel) slackGo.ModalViewRequest {
	// モーダルタイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "課題登録", false, false)
	// 閉じるボタン
	closeText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "閉じる", false, false)
	// 登録ボタン
	submitText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "登録", false, false)

	// 課題名
	issueNameText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "タイトル", false, false)
	issueNamePlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "タイトルを入力してください", false, false)
	issueNameElement := slackGo.NewPlainTextInputBlockElement(issueNamePlaceholder, "issueName")
	issueNameBlock := slackGo.NewInputBlock("issueName", issueNameText, issueNameElement)

	// 課題内容
	issueContentText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "内容", false, false)
	issueContentPlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "内容を入力してください", false, false)
	issueContentElement := slackGo.NewPlainTextInputBlockElement(issueContentPlaceholder, "issueContent")
	// 最大1000文字
	issueContentElement.MaxLength = 1000
	issueContentElement.Multiline = true
	issueContentElement.InitialValue = message.Text
	issueContentBlock := slackGo.NewInputBlock("issueContent", issueContentText, issueContentElement)

	// チャネル
	channelOptions := generateChannelsOptionBlockObjects(channels)
	channelText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "チャンネル", false, false)
	channelElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeStatic, nil, "channel", channelOptions...)
	// 今選択されているチャンネル
	if currentChannel != "" {
		currentChannelText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "<#"+currentChannel+">", false, false)
		currentChannelOptionBlockObjects := slackGo.NewOptionBlockObject(currentChannel, currentChannelText)
		channelElement.InitialOption = currentChannelOptionBlockObjects
	}
	channelBlock := slackGo.NewInputBlock("channel", channelText, channelElement)

	// 担当者
	userText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "担当者", false, false)
	userElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeUser, userText, "user")
	userBlock := slackGo.NewInputBlock("user", userText, userElement)

	// 期限
	deadlineText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "期限", false, false)
	deadlineElement := slackGo.NewDatePickerBlockElement("deadline")
	deadlineElement.InitialDate = timezone.GetCurrentDate() // デフォルト値（今日の年月日）
	deadlineBlock := slackGo.NewInputBlock("deadline", deadlineText, deadlineElement)

	blocks := slackGo.Blocks{
		BlockSet: []slackGo.Block{
			issueNameBlock,
			issueContentBlock,
			channelBlock,
			userBlock,
			deadlineBlock,
		},
	}

	var modalRequest slackGo.ModalViewRequest
	modalRequest.Type = slackGo.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks
	modalRequest.CallbackID = "storeIssue"

	return modalRequest
}

// GenerateUpdateModal : 課題更新モーダルを生成
func GenerateUpdateModal(issue mongodb.Issue, channels []slackGo.Channel) slackGo.ModalViewRequest {
	// モーダルタイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "課題更新", false, false)
	// 閉じるボタン
	closeText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "閉じる", false, false)
	// 更新ボタン
	submitText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "更新", false, false)

	// 課題名
	issueNameText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "タイトル", false, false)
	issueNamePlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "タイトルを入力してください", false, false)
	issueNameElement := slackGo.NewPlainTextInputBlockElement(issueNamePlaceholder, "issueName")
	issueNameElement.InitialValue = issue.Title
	issueNameBlock := slackGo.NewInputBlock("issueName", issueNameText, issueNameElement)

	// 課題内容
	issueContentText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "内容", false, false)
	issueContentPlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "内容を入力してください", false, false)
	issueContentElement := slackGo.NewPlainTextInputBlockElement(issueContentPlaceholder, "issueContent")
	// 最大1000文字
	issueContentElement.MaxLength = 1000
	issueContentElement.Multiline = true
	issueContentElement.InitialValue = issue.Content
	issueContentBlock := slackGo.NewInputBlock("issueContent", issueContentText, issueContentElement)

	channelOptions := generateChannelsOptionBlockObjects(channels)
	channelText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "チャンネル", false, false)
	channelElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeStatic, nil, "channel", channelOptions...)
	// 今選択されているチャンネル
	currentChannelText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "<#"+issue.ChannelID+">", false, false)
	currentChannelOptionBlockObjects := slackGo.NewOptionBlockObject(issue.ChannelID, currentChannelText)
	channelElement.InitialOption = currentChannelOptionBlockObjects
	channelBlock := slackGo.NewInputBlock("channel", channelText, channelElement)

	// 担当者
	userText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "担当者", false, false)
	userElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeUser, userText, "user")
	userElement.InitialUser = issue.UserID
	userBlock := slackGo.NewInputBlock("user", userText, userElement)

	// 期限
	deadlineText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "期限", false, false)
	deadlineElement := slackGo.NewDatePickerBlockElement("deadline")
	deadlineElement.InitialDate = issue.Deadline.Format(timezone.DateLayout)
	deadlineBlock := slackGo.NewInputBlock("deadline", deadlineText, deadlineElement)

	// ステータス
	statusText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "ステータス", false, false)
	statusElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeStatic, statusText, "status", GenerateStatusBlockObjects()...)
	// 今選択されているセレクトボックス
	currentStatusText := slackGo.NewTextBlockObject(slackGo.PlainTextType, issue.Status, false, false)
	currentStatusOptionBlockObjects := slackGo.NewOptionBlockObject(issue.Status, currentStatusText)
	statusElement.InitialOption = currentStatusOptionBlockObjects
	statusBlock := slackGo.NewInputBlock("status", statusText, statusElement)

	blocks := slackGo.Blocks{
		BlockSet: []slackGo.Block{
			issueNameBlock,
			issueContentBlock,
			channelBlock,
			userBlock,
			deadlineBlock,
			statusBlock,
		},
	}

	var modalRequest slackGo.ModalViewRequest
	modalRequest.Type = slackGo.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks
	modalRequest.CallbackID = "updateIssue_" + issue.DocID.Hex()

	return modalRequest
}

// GenerateSearchModal : 課題検索モーダルを生成
func GenerateSearchModal() slackGo.ModalViewRequest {
	// モーダルタイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "課題検索", false, false)
	// 閉じるボタン
	closeText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "閉じる", false, false)
	// 検索ボタン
	submitText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "検索", false, false)

	// キーワードを指定
	keywordText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "キーワード", false, false)
	keywordPlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "キーワードを入力してください", false, false)
	keywordElement := slackGo.NewPlainTextInputBlockElement(keywordPlaceholder, "keyword")
	keywordBlock := slackGo.NewInputBlock("keyword", keywordText, keywordElement)
	keywordBlock.Optional = true // 入力任意

	// 担当者を指定
	userText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "担当者", false, false)
	userElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeUser, userText, "user")
	userBlock := slackGo.NewInputBlock("user", userText, userElement)
	userBlock.Optional = true // 入力任意

	// ステータスを指定
	statusText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "ステータス", false, false)
	statusElement := slackGo.NewOptionsSelectBlockElement(slackGo.OptTypeStatic, statusText, "status", GenerateStatusBlockObjects()...)
	statusBlock := slackGo.NewInputBlock("status", statusText, statusElement)
	statusBlock.Optional = true // 入力任意

	blocks := slackGo.Blocks{
		BlockSet: []slackGo.Block{
			keywordBlock,
			userBlock,
			statusBlock,
		},
	}

	var modalRequest slackGo.ModalViewRequest
	modalRequest.Type = slackGo.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks
	modalRequest.CallbackID = "searchIssue"

	return modalRequest
}

// GenerateCommentModal : コメントモーダルを生成
func GenerateCommentModal(docID string, comments mongodb.Comments, userID string) slackGo.ModalViewRequest {
	// Divider
	divSectionBlock := slackGo.NewDividerBlock()

	// モーダルタイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメント", false, false)
	// 閉じるボタン
	closeText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "閉じる", false, false)
	// コメント登録ボタン
	submitText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメント登録", false, false)

	// 新規コメント
	newCommentContentText := slackGo.NewTextBlockObject(slackGo.PlainTextType, " ", false, false)
	newCommentContentPlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメントを入力してください", false, false)
	newCommentContentElement := slackGo.NewPlainTextInputBlockElement(newCommentContentPlaceholder, "newComment")
	// 最大1000文字
	newCommentContentElement.MaxLength = 1000
	newCommentContentElement.Multiline = true
	newCommentContentBlock := slackGo.NewInputBlock("newComment", newCommentContentText, newCommentContentElement)

	var tmpBlocks = []slackGo.Block{}
	// 取得したコメント分だけループする
	for _, comment := range comments {
		contentTextObject := slackGo.NewTextBlockObject(slackGo.MarkdownType, "Commented by <@"+comment.Creater+"> at "+comment.CreatedAt.In(time.Local).Format(timezone.DatetimeLayout)+"\n\n"+comment.Content, false, false)

		contentContextBlock := slackGo.NewContextBlock("", []slackGo.MixedElement{contentTextObject}...)

		var tmpBlock []slackGo.Block
		if userID == comment.Creater {
			// 編集ボタン
			editBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "編集", false, false)
			editBtnElement := slackGo.NewButtonBlockElement("editComment", comment.DocID.Hex(), editBtnText)

			// 削除ボタン
			deleteBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "削除", false, false)
			deleteBtnElement := slackGo.NewButtonBlockElement("deleteComment", comment.DocID.Hex(), deleteBtnText)

			fieldAction := slackGo.NewActionBlock("", editBtnElement, deleteBtnElement)

			tmpBlock = []slackGo.Block{
				contentContextBlock,
				fieldAction,
				divSectionBlock,
			}
		} else {
			tmpBlock = []slackGo.Block{
				contentContextBlock,
				divSectionBlock,
			}
		}

		tmpBlocks = append(tmpBlocks, tmpBlock...)
	}

	blocks := slackGo.Blocks{
		BlockSet: append(tmpBlocks, []slackGo.Block{
			newCommentContentBlock,
		}...),
	}

	var modalRequest slackGo.ModalViewRequest
	modalRequest.Type = slackGo.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks
	modalRequest.CallbackID = "storeComment_" + docID

	return modalRequest
}

// GenerateEditCommentModal : コメント編集モーダルを生成
func GenerateEditCommentModal(comment mongodb.Comment) slackGo.ModalViewRequest {
	// モーダルタイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメント編集", false, false)
	// 閉じるボタン
	closeText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "閉じる", false, false)
	// コメント更新ボタン
	submitText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "更新", false, false)

	// 編集コメント
	editCommentContentText := slackGo.NewTextBlockObject(slackGo.PlainTextType, " ", false, false)
	editCommentContentPlaceholder := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメントを入力してください", false, false)
	editCommentContentElement := slackGo.NewPlainTextInputBlockElement(editCommentContentPlaceholder, "editComment")
	// 最大1000文字
	editCommentContentElement.MaxLength = 1000
	editCommentContentElement.Multiline = true
	editCommentContentElement.InitialValue = comment.Content
	editCommentContentBlock := slackGo.NewInputBlock("editComment", editCommentContentText, editCommentContentElement)

	blocks := slackGo.Blocks{
		BlockSet: []slackGo.Block{
			editCommentContentBlock,
		},
	}

	var modalRequest slackGo.ModalViewRequest
	modalRequest.Type = slackGo.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks
	modalRequest.CallbackID = "updateComment_" + comment.DocID.Hex()

	return modalRequest
}

// GenerateDeleteIssueConfirmModal : 課題削除確認モーダルを生成
func GenerateDeleteIssueConfirmModal(hex string, content string) slackGo.ModalViewRequest {
	return generateDeleteModal(
		"課題削除確認",
		"下記の課題を削除しますが、よろしいですか？",
		"deleteIssue_"+hex,
		content,
	)
}

// GenerateDeleteCommentConfirmModal : コメント削除確認モーダルを生成
func GenerateDeleteCommentConfirmModal(hex string, content string) slackGo.ModalViewRequest {
	return generateDeleteModal(
		"コメント削除確認",
		"下記のコメントを削除しますが、よろしいですか？",
		"deleteComment_"+hex,
		content,
	)
}

// 削除確認モーダルを生成
func generateDeleteModal(title string, message string, callbackID string, content string) slackGo.ModalViewRequest {
	// Divider
	divSectionBlock := slackGo.NewDividerBlock()

	// モーダルタイトル
	titleText := slackGo.NewTextBlockObject(slackGo.PlainTextType, title, false, false)
	// 閉じるボタン
	closeText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "閉じる", false, false)
	// 削除ボタン
	submitText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "削除", false, false)

	// メッセージ
	messageText := slackGo.NewTextBlockObject(slackGo.PlainTextType, message, false, false)
	messageSection := slackGo.NewSectionBlock(messageText, nil, nil)

	contentTextObject := slackGo.NewTextBlockObject(slackGo.MarkdownType, "\n\n"+content, false, false)
	contentContextBlock := slackGo.NewContextBlock("", []slackGo.MixedElement{contentTextObject}...)

	blocks := slackGo.Blocks{
		BlockSet: []slackGo.Block{
			messageSection,
			divSectionBlock,
			contentContextBlock,
		},
	}

	var modalRequest slackGo.ModalViewRequest
	modalRequest.Type = slackGo.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks
	modalRequest.CallbackID = callbackID

	return modalRequest
}
