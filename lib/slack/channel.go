package lib

import (
	"os"

	slackGo "github.com/slack-go/slack"
)

// GetChannels : 課題担当者の所属するチャンネル情報を取得
func GetChannels(userID string) ([]slackGo.Channel, error) {
	// ユーザの所属するチャンネル情報を取得するのに利用
	userAuthAPI := slackGo.New(os.Getenv("SLACK_USER_AUTH_TOKEN"))

	conversationParams := slackGo.GetConversationsForUserParameters{
		UserID: userID,
		Cursor: "",
		Types: []string{
			"public_channel",
			"private_channel",
		},
		Limit:           100,
		ExcludeArchived: false,
	}

	channels, _, err := userAuthAPI.GetConversationsForUser(&conversationParams)
	if err != nil {
		return nil, err
	}

	return channels, nil
}

// GenerateChannelIDsMap : 取得したチャンネル情報からmapを生成
func GenerateChannelIDsMap(channels []slackGo.Channel) map[string]string {
	channelIDs := make(map[string]string, 0)
	for _, channel := range channels {
		channelIDs[channel.ID] = channel.ID
	}
	return channelIDs
}
