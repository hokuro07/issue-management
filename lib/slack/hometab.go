package lib

import (
	"time"

	slackGo "github.com/slack-go/slack"
	mongodb "gitlab.com/hokuro07/issue-management/lib/mongodb"
	timezone "gitlab.com/hokuro07/issue-management/lib/timezone"
)

// GenerateHomeTab : 課題管理タブを生成
func GenerateHomeTab(issues mongodb.Issues, channelIDs map[string]string, params map[string]map[string]slackGo.BlockAction) slackGo.HomeTabViewRequest {
	// blankText
	blankText := slackGo.NewTextBlockObject(slackGo.PlainTextType, " ", false, false)

	// Divider
	divSectionBlock := slackGo.NewDividerBlock()

	// メニュー部
	menuText := slackGo.NewTextBlockObject(slackGo.MarkdownType, " ", false, false)
	if params != nil {
		keyword := params["keyword"]["keyword"].Value
		userID := params["user"]["user"].SelectedUser
		status := params["status"]["status"].SelectedOption.Value

		//
		// 検索結果のテキスト
		//
		result := "*検索条件*\n"
		// キーワード
		if len(keyword) > 0 {
			// キーワードが指定された場合
			result += "• キーワード : `" + keyword + "`\n"
		} else {
			// キーワードが指定された場合
			result += "• キーワード : `nil`\n"
		}
		// ユーザ
		if len(userID) > 0 {
			// ユーザが指定された場合
			result += "• 担当者　　 : `<@" + userID + ">`\n"
		} else {
			// ユーザが指定されていない場合
			result += "• 担当者　　 : `nil`\n"
		}
		// ステータス
		if len(status) > 0 {
			// ステータスが指定された場合
			result += "• ステータス : `" + status + "`\n"
		} else {
			// ステータスが指定されていない場合
			result += "• ステータス : `nil`\n"
		}

		menuText = slackGo.NewTextBlockObject(slackGo.MarkdownType, result, false, false)
	} else {
		result := "*検索条件*\n"
		result += "• キーワード : `nil`\n"
		result += "• 担当者　　 : `nil`\n"
		result += "• ステータス : `nil`\n"

		menuText = slackGo.NewTextBlockObject(slackGo.MarkdownType, result, false, false)
	}
	searchOverflowOptionText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "検索", false, false)
	createOverflowOptionText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "作成", false, false)
	searchOverflowOption := slackGo.NewOptionBlockObject("searchIssue", searchOverflowOptionText)
	createOverflowOption := slackGo.NewOptionBlockObject("createIssue", createOverflowOptionText)
	menuOverflow := slackGo.NewOverflowBlockElement("menu", searchOverflowOption, createOverflowOption)
	menuSection := slackGo.NewSectionBlock(menuText, nil, slackGo.NewAccessory(menuOverflow))

	var tmpBlocks = []slackGo.Block{}
	// 取得した課題分だけループする
	for _, issue := range issues {
		// タイトル
		titleTextObject := slackGo.NewTextBlockObject(slackGo.PlainTextType, issue.Title, false, false)
		titleTextBlock := slackGo.NewHeaderBlock(titleTextObject)

		// 内容
		contentText := "Created by <@" + issue.Creater + "> at " + issue.CreatedAt.In(time.Local).Format(timezone.DatetimeLayout)
		contentText += "　　Last updated by <@" + issue.Updater + "> at " + issue.UpdatedAt.In(time.Local).Format(timezone.DatetimeLayout)
		contentText += "\n\n" + issue.Content + "\n"
		contentTextObject := slackGo.NewTextBlockObject(slackGo.MarkdownType, contentText, false, false)
		contentContextBlock := slackGo.NewContextBlock("", []slackGo.MixedElement{contentTextObject}...)

		// フィールドを分割して配置
		fieldSlice := make([]*slackGo.TextBlockObject, 0)

		// チャンネル
		channelField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• チャンネル : `<#"+issue.ChannelID+">`", false, false)
		fieldSlice = append(fieldSlice, channelField)
		// ステータス
		statusField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• ステータス : `"+issue.Status+"`", false, false)
		fieldSlice = append(fieldSlice, statusField)
		// 期限
		deadlineField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• 期限　　　 : `"+issue.Deadline.Format(timezone.DateLayout)+"`", false, false)
		fieldSlice = append(fieldSlice, deadlineField)
		// 担当者
		userField := slackGo.NewTextBlockObject(slackGo.MarkdownType, "• 担当者　　 : `<@"+issue.UserID+">`", false, false)
		fieldSlice = append(fieldSlice, userField)

		fieldSection := slackGo.NewSectionBlock(blankText, fieldSlice, nil)

		// 編集ボタン
		editBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "編集", false, false)
		editBtnElement := slackGo.NewButtonBlockElement("editIssue", issue.DocID.Hex(), editBtnText)

		// コメントボタン
		commentBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "コメント", false, false)
		commentBtnElement := slackGo.NewButtonBlockElement("comment", issue.DocID.Hex(), commentBtnText)

		// 削除ボタン
		deleteBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, " 削除", false, false)
		deleteBtnElement := slackGo.NewButtonBlockElement("deleteIssue", issue.DocID.Hex(), deleteBtnText)

		// 課題に紐づくチャンネルにボットが参加している場合に限り、共有ボタンを表示する
		var fieldAction = &slackGo.ActionBlock{}
		if _, ok := channelIDs[issue.ChannelID]; ok {
			// 共有ボタン
			shareBtnText := slackGo.NewTextBlockObject(slackGo.PlainTextType, "共有", false, false)
			shareBtnElement := slackGo.NewButtonBlockElement("share", issue.DocID.Hex(), shareBtnText)
			fieldAction = slackGo.NewActionBlock("", editBtnElement, commentBtnElement, shareBtnElement, deleteBtnElement)
		} else {
			fieldAction = slackGo.NewActionBlock("", editBtnElement, commentBtnElement, deleteBtnElement)
		}

		tmpBlock := []slackGo.Block{
			titleTextBlock,
			contentContextBlock,
			fieldSection,
			fieldAction,
			divSectionBlock,
		}

		tmpBlocks = append(tmpBlocks, tmpBlock...)
	}

	blocks := slackGo.Blocks{
		BlockSet: append([]slackGo.Block{
			menuSection,
			divSectionBlock,
		}, tmpBlocks...),
	}

	var hometabRequest slackGo.HomeTabViewRequest
	hometabRequest.Type = slackGo.ViewType(slackGo.VTHomeTab)
	hometabRequest.Blocks = blocks

	return hometabRequest
}
