package lib

import (
	slackGo "github.com/slack-go/slack"
)

// GenerateStatusBlockObjects : ステータスのselectボックスを生成
func GenerateStatusBlockObjects() []*slackGo.OptionBlockObject {
	status := []string{"未着手", "着手中", "完了", "保留"}

	optionBlockObjects := make([]*slackGo.OptionBlockObject, 0, len(status))
	for _, v := range status {
		optionText := slackGo.NewTextBlockObject(slackGo.PlainTextType, v, false, false)
		optionBlockObjects = append(optionBlockObjects, slackGo.NewOptionBlockObject(v, optionText))
	}

	return optionBlockObjects
}
