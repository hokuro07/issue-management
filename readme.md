# Slack課題管理APP
Slackのホームタブ、モーダル、通知の機能を利用した課題管理アプリケーションです。  
課題管理として作成しましたが、TODO管理などにも利用できると思います。  

# デモ
## ホームタブ
課題の検索、作成、編集、削除や、コメントの投稿、編集などが行えます。  

![ホームタブデモ](https://gitlab.com/hokuro07/issue-management/uploads/9de578f0e6f05fc82ce0207658087778/slack課題管理appデモ1.gif "ホームタブデモ")

## 課題の共有
課題に紐づくチャンネルに、課題を共有することができます。  
共有先のチャンネルでも、課題の編集やコメントの投稿が可能です。  

![課題の共有デモ](https://gitlab.com/hokuro07/issue-management/uploads/1e79d8dd5435c4c9c23f189a620d1f8a/slack課題管理appデモ2.gif "課題作成モーダル")

## 各チャンネルから課題の作成
各チャンネルのメッセージショートカットからも課題を作成することができます。

![メッセージショートカット課題作成デモ](https://gitlab.com/hokuro07/issue-management/uploads/47f272d2ccef142ca7b7b01c90b58384/slack課題管理appデモ3.gif "メッセージショートカット課題作成デモ")

# Go言語バージョン
go version go1.13.x

# 実行環境
下記3つの無料で整えられる環境の利用を想定して作りました。

* Google Cloud Functions
* MongoDB Atlas
* Google Cloud Scheduler (任意)

Cloud Functionsの環境変数に以下を設定し、SlackRequestを指定してデプロイしてください。

```
SLACK_BOT_TOKEN=BotユーザのOAuthアクセストークン
SLACK_BOT_USER_ID=課題管理APPのユーザID
SLACK_USER_AUTH_TOKEN=OAuthアクセストークン
SLACK_SIGNING_SECRET=Signing Secret
MONGODB_ID=MongoDBのID
MONGODB_PW=MongoDBのパスワード
MONGODB_HOSTNAME=ホスト名
MONGODB_DBNAME=DB名
```

ngrokなどを利用しローカルで実行することも可能です。  
.envをコピーし、.env.developを作成のうえ設定してください。  
その後、対象ディレクトへ移動し、下記コマンドを実行することで起動可能です。
```
go run develop/local.go
```
   
Slackの設定等は一旦割愛します。  
需要があれば記載します。

# 注意
本ツール使用上起きた問題についての責任は負いかねます。  
自己責任で利用してください。

# ライセンス 
`Slack課題管理APP` は[MIT license](https://en.wikipedia.org/wiki/MIT_License)で配布しています。
