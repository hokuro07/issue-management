package http

import (
	"net/http"
	"strings"

	handler "gitlab.com/hokuro07/issue-management/lib/handler"
	timezone "gitlab.com/hokuro07/issue-management/lib/timezone"
)

func init() {
	// Asia/Tokyo をセットする
	timezone.SetJpTimeZone()
}

// SlackRequest : HTTP Cloud Function
func SlackRequest(w http.ResponseWriter, r *http.Request) {
	if strings.Contains(r.RequestURI, "/issue/modal") {
		handler.HandleModal(w, r)
	} else if strings.Contains(r.RequestURI, "/issue/event") {
		handler.HandleHome(w, r)
	} else if strings.Contains(r.RequestURI, "/issue/notice") {
		handler.HandleNotice(w, r)
	}
}
