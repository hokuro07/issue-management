module gitlab.com/hokuro07/issue-management

go 1.13

require (
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/slack-go/slack v0.6.6
	go.mongodb.org/mongo-driver v1.4.1
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
)
