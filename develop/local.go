package main

import (
	"log"
	"net/http"

	godotenv "github.com/joho/godotenv"
	handler "gitlab.com/hokuro07/issue-management/lib/handler"
	timezone "gitlab.com/hokuro07/issue-management/lib/timezone"
)

func init() {
	// Asia/Tokyo をセットする
	timezone.SetJpTimeZone()
}

// ngrok等でローカル実行する際に使用
func main() {
	// envを読み込む
	err := godotenv.Load("./.env.develop")
	if err != nil {
		log.Println(err)
		return
	}

	http.HandleFunc("/issue/modal", handler.HandleModal)
	http.HandleFunc("/issue/event", handler.HandleHome)
	http.HandleFunc("/issue/notice", handler.HandleNotice)

	log.Println("[INFO] Server listening")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
